package com.hoo.common.mock;

import java.util.logging.Logger;

/**
 * 测试类
 *
 * @author hank
 * @create 2020-07-25 下午12:39
 **/

public class MockDbUtilTests {

    static Logger logger = Logger.getLogger(MockDbUtilTests.class.getName());

    public static void main(String[] args) {
        //logger.info("moment1:" + MockDbUtil.parse("Mock.mock(\"@moment('yyyy')\")"));
        logger.info("moment2:" + MockDbUtil.parse("Mock.mock('@moment(yyyy/MM/dd HH:mm:ss.S, 0, 0 , -30)')")); // Ok!
        //logger.info("moment3:" + MockDbUtil.parse("Mock.mock('@moment')"));
        //logger.info("moment4:" + MockDbUtil.parse(String.format("Mock.mock('%s')", "@moment()")));
        logger.info("moment5:" + MockDbUtil.parse("@moment()"));
        logger.info("email:" + MockDbUtil.parse("@email"));
        logger.info("name:" + MockDbUtil.parse("@name"));
        logger.info("integer:" + MockDbUtil.parse("@integer"));
    }

}
