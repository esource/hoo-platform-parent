// 需压缩, 符合格式语法
Mock.Random.extend({
  /**
   * @example
   * moment('yyyy-MM-dd')  // 2020-01-01
   * moment('yy/MM/dd HH:mm:ss')  // 20-01-01 12:12:11
   * moment('yyyy-MM-dd HH:mm:ss', 0, 0, 0, -1) // 返回当前时间 晚 1 秒 的字符串
   * @returns {string}
   */
  moment: function () {
    var now = new Date()
    var config = {
      fmt: typeof arguments[0] === 'string' ? arguments[0] : 'yyyy-MM-dd HH:mm:ss',
      day: typeof arguments[1] === 'string' ? Number(arguments[1]) : 0,
      hour: typeof arguments[2] === 'string' ? Number(arguments[2]) : 0,
      minute: typeof arguments[3] === 'string' ? Number(arguments[3]) : 0,
      second: typeof arguments[4] === 'string' ? Number(arguments[4]) : 0
    };
    // 日期操作
    var time = now.getTime();
    now.setTime(time + config.second * 1000 + config.minute * 60 * 1000 + config.hour * 60 * 60 * 1000 + config.day * 24 * 60 * 60 * 1000)
    var o = {
      "M+" : now.getMonth() + 1,               //月份
      "d+" : now.getDate(),                    //日
      "h+" : now.getHours(),                   //小时
      "H+" : now.getHours(),
      "m+" : now.getMinutes(),                 //分
      "s+" : now.getSeconds(),                 //秒
      "q+" : Math.floor((now.getMonth()+3)/3), //季度
      "S+"  : now.getMilliseconds()             //毫秒
    };
    var fmt = config.fmt;
    if(/(y+)/.test(fmt)){
      fmt=fmt.replace(RegExp.$1, ((now.getFullYear())+"").substr(4 - RegExp.$1.length));
    }
    for(var k in o) {
      if(new RegExp("("+ k +")").test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
      }
    }
    return fmt;
  }
});