package com.hoo.sso.desktopclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SsoDesktopClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(SsoDesktopClientApplication.class, args);
    }

}
