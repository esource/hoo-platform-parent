package com.example.hoo.permission.jwt.sdk;

import com.alibaba.fastjson.JSON;
import com.hoo.permission.sdk.web.config.SafeConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 配置测试
 *
 * @author hank
 * @create 2020-11-13 下午6:08
 **/
@SpringBootTest
public class ConfigTest {

    @Autowired SafeConfig config;

    // @Test
    public void test(){
        System.out.println(JSON.toJSONString(config));
    }

}
