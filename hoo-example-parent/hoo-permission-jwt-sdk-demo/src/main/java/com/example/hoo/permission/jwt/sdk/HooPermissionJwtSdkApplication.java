package com.example.hoo.permission.jwt.sdk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.oas.annotations.EnableOpenApi;

/**
 * @author hank
 */
@SpringBootApplication(scanBasePackages = {"com.example.hoo.permission.jwt.sdk", "com.hoo.permission.sdk"})
public class HooPermissionJwtSdkApplication {

    public static void main(String[] args) {
        SpringApplication.run(HooPermissionJwtSdkApplication.class, args);
    }

}
