import store from '@/store'
/**
 * 过滤当前用户的权限中是否包含指定权限
 * @param actionName
 */
export function hasPermission (actionName) {
  const permissions = store.getters.permissions || []
  if (actionName && permissions.includes(actionName)) {
    return true
  }
}

/**
 * 判断指定权限中是否有任意一个权限在当前用户权限集中
 * @param actionNames 逗号分隔的权限标记
 * @returns {boolean}
 */
export function hasAnyPermission(actionNames) {
  let include = false
  const permissions = store.getters.permissions || []
  for (const actionName in (actionNames || '').split(',')) {
    include = permissions.includes(actionName)
    if (include) {
      break
    }
  }
  return include
}