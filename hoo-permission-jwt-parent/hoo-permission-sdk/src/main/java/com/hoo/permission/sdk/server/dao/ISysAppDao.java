package com.hoo.permission.sdk.server.dao;

import com.hoo.common.model.Page;
import com.hoo.permission.sdk.server.domain.entity.SysApp;
import com.hoo.permission.sdk.server.domain.pojo.SysAppPo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 系统接入应用表，用于支持SSO多平台权限控制 Dao层接口
 * @author 小韩工作室
 * @date 2020-08-09 17:19:00
 */
public interface ISysAppDao {
    /**
     * 新增 - 系统接入应用表，用于支持SSO多平台权限控制
     */
    boolean add(SysApp entity);

    /**
     * 修改 - 系统接入应用表，用于支持SSO多平台权限控制
     */
    boolean update(SysApp entity);

    /**
     * 删除 - 系统接入应用表，用于支持SSO多平台权限控制
     */
    boolean delete(SysApp entity);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    boolean batchDelete(List<Long> ids);

    /**
     * 根据主键 获取记录
     */
    SysApp get(Serializable id);

    /**
     * 根据条件查询所有记录
     */
    List<SysApp> findAll(Map< String, Object> params);

    /**
     * 分页查询处理
     * @param page
     * @param params
     * @return
     */
    Page<SysApp> query(Page page, SysAppPo params);
}
