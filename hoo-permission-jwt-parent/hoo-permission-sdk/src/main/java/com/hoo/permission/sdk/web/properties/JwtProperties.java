package com.hoo.permission.sdk.web.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.util.StringUtils;

/**
 * jwt 配置
 *
 * @author hank
 * @create 2020-11-12 下午4:47
 **/
@ConfigurationProperties(prefix = "hoo.permission.jwt")
public class JwtProperties {
    /**
     * 密钥
     */
    private String secret;
    /**
     * 有效期(单位：秒), 默认 2小时
     */
    private Long expire;
    /**
     * header中取值name
     */
    private String headerName;

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public Long getExpire() {
        return (expire == null || expire < 0L) ? 7200 : expire ;
    }

    public void setExpire(Long expire) {
        this.expire = expire;
    }

    public String getHeaderName() {
        return StringUtils.isEmpty(headerName) ? "Authorization" : headerName;
    }

    public void setHeaderName(String headerName) {
        this.headerName = headerName;
    }

}
