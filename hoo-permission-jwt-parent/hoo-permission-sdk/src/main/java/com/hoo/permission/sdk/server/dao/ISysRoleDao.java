package com.hoo.permission.sdk.server.dao;

import com.hoo.common.model.Page;
import com.hoo.permission.sdk.server.domain.dto.SysRoleDto;
import com.hoo.permission.sdk.server.domain.entity.SysRole;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 系统-角色表 Dao层接口
 * @author 小韩工作室
 * @date 2020-06-28 16:51:32
 */
public interface ISysRoleDao {
    /**
     * 新增 - 系统-角色表
     */
    boolean add(SysRole entity);

    /**
     * 修改 - 系统-角色表
     */
    boolean update(SysRole entity);


    /**
     * 变更状态
     * @param roleId
     * @param status
     * @return
     */
    boolean updateStatus(Long roleId, String status);

    /**
     * 删除 - 系统-角色表
     * @param entity
     * @return
     */
    boolean delete(SysRole entity);

    /**
     * 删除 - 系统-角色表
     * @param roleId
     * @return
     */
    boolean delete(Long roleId);

    /**
     * 根据主键 获取记录
     */
    SysRole get(Serializable id);

    /**
     * 根据条件查询所有记录
     */
    List<SysRole> findAll(Map<String, Object> params);

    /**
     * 分页查询处理
     * @param page
     * @param params
     * @return
     */
    Page<SysRoleDto> query(Page page, Map<String, Object> params);
}
