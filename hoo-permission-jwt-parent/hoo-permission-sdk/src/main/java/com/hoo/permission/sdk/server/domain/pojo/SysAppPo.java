package com.hoo.permission.sdk.server.domain.pojo;

import com.hoo.permission.sdk.server.domain.entity.SysApp;

/**
 * 系统应用PO层 用于前端业务传参解析
 *
 * @author hank
 * @create 2020-08-12 上午8:11
 **/
public class SysAppPo extends SysApp {
}
