package com.hoo.permission.sdk.server.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 登录用户简易模型
 *
 * @author hank
 * @create 2020-11-12 下午4:04
 **/

public class LoginUser {
    private Long id;
    private String username;
    private String password;

    public LoginUser(){
        super();
    }

    public LoginUser(Long id, String username) {
        this.id = id;
        this.username = username;
    }

    public LoginUser(Long id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
