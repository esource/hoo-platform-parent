package com.hoo.permission.sdk.server.domain.entity;

import java.io.Serializable;

/**
 * 系统-角色用户关系表
 * @author 小韩工作室
 * @date 2020-06-28 18:15:48
 */
public class SysRoleUser implements Serializable {
    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 角色ID
     */
    private Long roleId;

    public Long getUserId(){
        return this.userId;
    }

    public Long getRoleId(){
        return this.roleId;
    }

    public void setUserId(Long userId){
        this.userId = userId;
    }

    public void setRoleId(Long roleId){
        this.roleId = roleId;
    }

}
