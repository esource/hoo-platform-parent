package com.hoo.permission.sdk.server.domain.dto;

import com.hoo.permission.sdk.server.domain.entity.SysRole;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 系统-角色 dto 模型
 *
 * @author hank
 * @create 2020-10-20 上午9:44
 **/

public class SysRoleDto extends SysRole {

    /**
     * 逗号分隔的资源ids
     */
    private String resourceIdStr;
    /**
     * 资源ID集合
     */
    private List<Long> resourceIds;

    public String getResourceIdStr() {
        return resourceIdStr;
    }

    public void setResourceIdStr(String resourceIdStr) {
        this.resourceIdStr = resourceIdStr;
    }

    public List<Long> getResourceIds() {
        if(resourceIds == null) {
            resourceIds = new ArrayList<>();
            for (String id : (null != resourceIdStr ? resourceIdStr : "").split(",")) {
                if(StringUtils.isEmpty(id)) { continue; }
                resourceIds.add(Long.valueOf(id));
            }
        }
        return resourceIds;
    }

    public void setResourceIds(List<Long> resourceIds) {
        this.resourceIds = resourceIds;
    }
}
