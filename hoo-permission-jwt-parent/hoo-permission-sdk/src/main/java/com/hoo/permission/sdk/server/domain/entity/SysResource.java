package com.hoo.permission.sdk.server.domain.entity;

import com.hoo.permission.sdk.server.domain.model.ResourceStatus;

import java.io.Serializable;

/**
 * 系统-资源表(菜单、按钮)
 * @author 小韩工作室
 * @date 2020-06-28 16:51:32
 */
public class SysResource implements Serializable {
    /**
     * ID主键
     */
    private Long id;
    /**
     * 父ID
     */
    private Long parentId;
    /**
     * 资源名称（菜单名称、按钮名称）
     */
    private String name;
    /**
     * 图标
     */
    private String icon;
    /**
     * 链接地址
     */
    private String url;
    /**
     * 组件路径(如VUE场景)
     */
    private String path;
    /**
     * 权限标识
     */
    private String permission;
    /**
     * 排序序号
     */
    private int sort;

    /**
     * 资源类型 1: 菜单，2：按钮
     * @see {com.hoo.permission.sdk.server.domain.model.ResourceType}
     */
    private int type;

    /**
     * 0 不隐藏， 1 隐藏
     */
    private boolean hide;

    /**
     * 资源状态 1：可用，0：禁用
     * @see {com.hoo.permission.sdk.server.domain.model.ResourceStatus}
     */
    private String status;

    public Long getId(){
        return this.id;
    }

    public Long getParentId(){
        return this.parentId;
    }

    public String getName(){
        return this.name;
    }

    public String getIcon(){
        return this.icon;
    }

    public String getUrl(){
        return this.url;
    }

    public String getPath(){
        return this.path;
    }

    public String getPermission(){
        return this.permission;
    }

    public int getSort(){
        return this.sort;
    }

    public void setId(Long id){
        this.id = id;
    }

    public void setParentId(Long parentId){
        this.parentId = parentId;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setIcon(String icon){
        this.icon = icon;
    }

    public void setUrl(String url){
        this.url = url;
    }

    public void setPath(String path){
        this.path = path;
    }

    public void setPermission(String permission){
        this.permission = permission;
    }

    public void setSort(int sort){
        this.sort = sort;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isHide() {
        return hide;
    }

    public void setHide(boolean hide) {
        this.hide = hide;
    }

    public String getStatus() {
        return status == null ? ResourceStatus.NORMAL :status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
