package com.hoo.permission.sdk.server.domain.entity;

import java.io.Serializable;

/**
 * 系统用户-资源关系表（适用于弱角色场景、减少角色处理场景)
 * @author 小韩工作室
 * @date 2020-08-09 17:17:56
 */
public class SysUserResource implements Serializable {
    /**
     * 用户ID
     */
    private Integer userId;
    /**
     * 资源ID
     */
    private Integer resourceId;

    public Integer getUserId(){
        return this.userId;
    }

    public Integer getResourceId(){
        return this.resourceId;
    }

    public void setUserId(Integer userId){
        this.userId = userId;
    }

    public void setResourceId(Integer resourceId){
        this.resourceId = resourceId;
    }

}
