package com.hoo.permission.sdk.web.api.system;

import com.hoo.common.model.Page;
import com.hoo.common.model.R;
import com.hoo.permission.sdk.server.domain.entity.SysUser;
import com.hoo.permission.sdk.server.domain.model.LoginUser;
import com.hoo.permission.sdk.server.domain.pojo.SysUserPo;
import com.hoo.permission.sdk.server.manager.IPermissionService;
import com.hoo.permission.sdk.server.service.ISysUserService;
import com.hoo.permission.sdk.server.util.StringUtil;
import com.hoo.permission.sdk.web.security.shiro.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户控制器
 *
 * @author hank
 * @create 2020-06-29 下午5:12
 **/
@Api(tags = "用户管理", description = "用户登录信息和CRUD功能")
@RestController
@RequestMapping("user")
public class UserController {
    // 新增、修改、删除用户这里提供基础的，接入系统可根据实际情况使用

    @Autowired IPermissionService permissionService;
    @Autowired ISysUserService userService;

    /**
     * 用户信息
     * @return
     */
    @ApiOperation("获取登录者自身信息")
    @GetMapping("/info")
    public R info() {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        if(loginUser == null) {
            throw new SecurityException("非法请求");
        }

        SysUser user = userService.getByUserName(loginUser.getUsername());
        Map<String,Object> data = new HashMap<>(3);
        data.put("user", user);
        data.put("perms", permissionService.getPermissions(user.getId()));
        data.put("roles", permissionService.getRoles(user.getId()));
        // data.put("menus", permissionService.getMenus(user.getId()));
        return R.data(data);
    }

    @ApiOperation("用户信息")
    @GetMapping
    @RequiresPermissions("sys:user:list")
    public R query(Integer pageNo, Integer pageSize, String searchWord, String status) {
        Map<String,Object> params = new HashMap<>();
        params.put("searchWord", searchWord);
        params.put("status", status);
        return R.page(userService.query(new Page(pageNo, pageSize), params));
    }

    @ApiOperation("新增用户")
    @PostMapping
    @RequiresPermissions("sys:user:save")
    public R save(SysUserPo userDto) {
        try {
            return R.data(userService.save(userDto));
        }catch (DuplicateKeyException e){
            return R.error(userDto.getUsername() + "已存在，不可重复添加");
        }
    }

    @ApiOperation("修改用户状态")
    @PutMapping("/updateStatus")
    @RequiresPermissions({"sys:user:useable", "sys:user:disabled", "sys:user:locked", "sys:user:unlock"})
    public R updateStatus(Long id, int status) {
        return R.data(userService.updateStatus(id, status));
    }

    @ApiOperation("修改用户")
    @PutMapping
    @RequiresPermissions("sys:user:update")
    public R update(SysUserPo userDto) {
        userService.update(userDto);
        return R.ok();
    }

    @ApiOperation("删除用户")
    @DeleteMapping("/{ids}")
    @RequiresPermissions("sys:user:delete")
    public R delete(@PathVariable String ids) {
        List<Long> longList = StringUtil.parseLong(ids);
        LoginUser loginUser = SecurityUtils.getLoginUser();
        if(longList.contains(loginUser.getId())) {
            return R.error("不可删除自己！");
        }
        userService.batchDelete(longList);
        return R.ok();
    }
}
