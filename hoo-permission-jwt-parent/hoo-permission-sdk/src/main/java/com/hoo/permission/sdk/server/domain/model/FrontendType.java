package com.hoo.permission.sdk.server.domain.model;

/**
 * 前端类型
 *
 * @author hank
 * @create 2020-08-09 17:19:00
 */
public interface FrontendType {
    /**
     * vue-cli
     */
    String VUE = "vue";

    /**
     * 自定义
     */
    String CUSTOM = "custom";
}
