package com.hoo.permission.sdk.server.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

/**
 * 系统-用户表
 * @author 小韩工作室
 * @date 2020-06-28 16:51:32
 */
public class SysUser implements Serializable {
    /**
     * ID主键
     */
    private Long id;
    /**
     * 昵称、真实姓名
     */
    private String nickname;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    @JsonIgnore
    private String password;
    /**
     * 盐值
     */
    @JsonIgnore
    private String salt;
    /**
     * 邮箱地址
     */
    private String email;
    /**
     * 手机号(预留位数)
     */
    private String mobile;
    /**
     * 性别: UNKNOW、MALE、FEMALE
     * @see {com.hoo.permission.sdk.server.domain.model.UserSex}
     */
    private String sex;
    /**
     * 当前状态（0 禁用，1 可用, 2 锁定）
     * @see {com.hoo.permission.sdk.server.domain.model.UserStatus}
     */
    private int status;

    public Long getId(){
        return this.id;
    }

    public String getNickname(){
        return this.nickname;
    }

    public String getUsername(){
        return this.username;
    }

    public String getPassword(){
        return this.password;
    }

    public String getSalt(){
        return this.salt;
    }

    public String getEmail(){
        return this.email;
    }

    public String getMobile(){
        return this.mobile;
    }

    public String getSex(){
        return this.sex;
    }

    public Integer getStatus(){
        return this.status;
    }

    public void setId(Long id){
        this.id = id;
    }

    public void setNickname(String nickname){
        this.nickname = nickname;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public void setPassword(String password){
        this.password = password;
    }

    public void setSalt(String salt){
        this.salt = salt;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public void setMobile(String mobile){
        this.mobile = mobile;
    }

    public void setSex(String sex){
        this.sex = sex;
    }

    public void setStatus(Integer status){
        this.status = status;
    }

}
