package com.hoo.permission.sdk.web.api.system;

import com.hoo.common.model.Page;
import com.hoo.common.model.R;
import com.hoo.permission.sdk.server.domain.entity.SysApp;
import com.hoo.permission.sdk.server.domain.pojo.SysAppPo;
import com.hoo.permission.sdk.server.service.ISysAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 应用控制器
 *
 * @author hank
 * @create 2020-08-11 下午11:26
 **/
@RestController
@RequestMapping("app")
public class AppController {

    @Autowired ISysAppService appService;

    @PostMapping
    public R add(SysApp entity) {
        entity.setId(null);
        return R.data(appService.save(entity));
    }

    @PutMapping
    public R update(SysApp entity) {
        return R.data(appService.update(entity));
    }

    @DeleteMapping("/{ids}")
    public R delete(@PathVariable("ids") String ids){
        if(StringUtils.isEmpty(ids)){
            return R.error("参数不合法");
        }
        List<Long> args = new ArrayList();
        for(String id : ids.split(",")){
            args.add(Long.valueOf(id));
        }
        return R.data(appService.delete(args));
    }

    @GetMapping
    public R page(Page page, SysAppPo params){
        return R.page(appService.query(page, params));
    }

}
