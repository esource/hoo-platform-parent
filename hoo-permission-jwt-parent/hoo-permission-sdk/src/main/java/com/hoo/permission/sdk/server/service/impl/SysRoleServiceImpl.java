package com.hoo.permission.sdk.server.service.impl;

import com.hoo.permission.sdk.server.dao.ISysRoleDao;
import com.hoo.permission.sdk.server.dao.ISysRoleResourceDao;
import com.hoo.permission.sdk.server.dao.ISysRoleUserDao;
import com.hoo.permission.sdk.server.domain.model.RoleStatus;
import com.hoo.permission.sdk.server.domain.pojo.SysRolePo;
import com.hoo.permission.sdk.server.domain.entity.SysRole;
import com.hoo.permission.sdk.server.service.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * 角色服务层实现类
 *
 * @author hank
 * @create 2020-06-28 下午9:17
 **/
@Service
public class SysRoleServiceImpl implements ISysRoleService {

    @Autowired ISysRoleDao roleDao;
    @Autowired ISysRoleResourceDao roleResourceDao;
    @Autowired ISysRoleUserDao roleUserDao;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(SysRolePo roleDto) {
        // 校验处理
        // 修改
        if(roleDto.getId() != null) {
            SysRole role = roleDao.get(roleDto.getId());
            if(role == null) {
                throw new IllegalArgumentException(String.format("the roleId (%d) is already exist !", roleDto.getId()));
            }
            roleDao.update(roleDto);
        } else {
            // 新增
            roleDao.add(roleDto);
        }
        // 处理角色和资源关联关系
        saveRoleResource(roleDto.getId(), roleDto.getResourceIds());
    }

    private void saveRoleResource(Long roleId, List<Long> resourceIds) {
        if (roleId == null) { return; }
        // 先删，再增( 先取当前的， 如果一致则不执行，否则按先删后增)
        List<Long> curResIds = roleResourceDao.queryResourceIds(roleId);
        if (curResIds != null && resourceIds != null) {
            Comparator sort = new Comparator<Long>() {
                @Override
                public int compare(Long o1, Long o2) {
                    return o1.compareTo(o2);
                }
            };
            curResIds.sort(sort);
            resourceIds.sort(sort);
            if (curResIds.equals(resourceIds)) {
                return;
            }
        }
        // list 标准排序后，比较即可
        if(!roleResourceDao.deleteByRoleId(roleId)) {
            // throw new RuntimeException(String.format("delete roleId(= %d) is fail !", roleId));
        }
        if(resourceIds != null && !CollectionUtils.isEmpty(resourceIds)) {
            if (!roleResourceDao.add(roleId, resourceIds)) {
                throw new RuntimeException(String.format("add 「roleId(%d) and resources(%s)」 is fail", roleId, resourceIds.toString()));
            }
        }
    }

    @Override
    public boolean updateStatus(Long id, String status) {
        if (!RoleStatus.DISABLED.equals(status)) {
            status = RoleStatus.NORMAL;
        }
        return roleDao.updateStatus(id, status);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(List<Long> roleIds) {
        for(Long roleId : roleIds) {
            if(roleId == null) { continue; }
            // 删除 角色用户关系、角色资源关系、自身
            roleUserDao.deleteByRoleId(roleId);
            roleResourceDao.deleteByRoleId(roleId);
            roleDao.delete(roleId);
        }
    }

    @Override
    public List<SysRole> queryAll(Map<String, Object> params) {
        return roleDao.findAll(params);
    }
}
