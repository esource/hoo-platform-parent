# Hoo 权管系统说明

```text
1、系统概述
	1.1 技术栈说明
		1.1.1 前端技术栈
		1.1.2 后端技术栈
	1.2 设计思路
	1.3 系统特色
	1.4 使用及系统展示
2、版本说明
	2.1 当前版本说明
	2.2 后续版本计划
```

## 一、系统概述

### 1.1 技术栈说明

系统（当前 release_simple 版本）本着易上手学习的思路，尽量避免使用过多的技术栈，以便读者减轻学习难度，让学习者更关注`权限管理`本身。

#### 1.1.1 前端技术栈

* Vue 2.0
* Ant Design Vue
* 基于 [vue-antd-pro](https://gitee.com/sendya/ant-design-pro-vue) 开发，删减和完善了一些功能

#### 1.1.2 后端技术栈

* SpringBoot 2.3.1.RELEASE
* Shiro 1.4.0
* Jwt 3.4.1
* 额外增加了 Druid 1.1.0、Swagger 3.0.0 用于监控和API管理

### 1.2 设计思路

系统基于通用的权限系统RBAC的思想设计。包含4个关键元素：用户-角色-权限-资源（关系见下图），系统通过以下表结构维护上述关系。

![用户-角色-权限-资源](assert/rbac.png)

| 表名                  | 说明                           |
| ------------------- | ---------------------------- |
| t_sys_user          | 用户表                          |
| t_sys_role_user     | 角色-用户关系表，维护用户分配的角色信息         |
| t_sys_role          | 角色表                          |
| t_sys_role_resource | 角色-资源关系表，维护角色拥有的资源关系信息       |
| t_sys_resource      | 资源表（含权限标识）                   |
| t_sys_user_resource | 用户-资源关系表，特殊保留的表，在某些场景可方便分配权限 |

### 1.3 系统特色

* 支持iframe

  配置方式： `组件地址固定值：AIframe`、`菜单URL配置为可访问的URL地址`。

* 支持外链

  配置方式：`组件地址建议为：EmptyLayout`、`菜单URL配置为可访问的URL地址`。

* 支持多角色（等同角色组）和独立权限

  单一用户可拥有多个角色，同时可以为用户单独设置资源权限，满足特殊需求（如主副领导，可不新建角色，单独分配权限即可）。

* 极少的三方依赖

  服务端使用 JdbcTemplate，手工SQL编写，更容易上手理解，适合基础学习。

  使用Shrio实现权限控制、Jwt实现安全授权。

  在demo模块引入Swagger作为API管理及Druid作为数据库监控。

* 前后端分离

  适合前后端从业人员学习，特别是前端非常棒。

  系统主要功能提供SDK，方便多个小项目直接引用，完成基础用户、资源及权限管理。

### 1.4 使用及系统展示

* 使用指南

  * 准备事宜

    * 新建数据库、导入SQL文件（/hoo-platform-parent/hoo-permission-jwt-parent/hoo-permission-sdk/src/main/resources/sql/release_simple.mysql.sql）。
    * 修改application配置中数据库连接信息（/hoo-platform-parent/hoo-example-parent/hoo-permission-jwt-sdk-demo/src/main/resources/application-dev.yml）

  * 启动

    * 服务端启动 HooPermissionJwtSdkApplication.java 文件。

    * 前端（/hoo-platform-parent/hoo-permission-jwt-parent/hoo-permission-jwt-ui）启动步骤，如下：

      ```text
      1、安装依赖： npm install
      2、启动： npm run serve
      3、打开：http://localhost:8000
      ```

  * 登录

    | 用户名      | 密码     | 角色权限           |
    | -------- | ------ | -------------- |
    | han.q    | 123456 | 超级管理员，读写权限等    |
    | han.qing | 123456 | 管理员，读权限，部分的写权限 |
    | hanqing2 | 123456 | 只读用户，仅读权限      |

* 系统展示

  ![登录页](assert/login.png)

  ![首页](assert/workplace.png)

  ![用户界面](assert/user.png)

  ![角色界面](assert/role.png)

  ![资源-菜单](assert/resource_menu.png)

  ![资源-按钮](assert/resource_btn.png)

  ![swagger3](assert/swagger3.png)

  ![druid](assert/druid_monitor.png)

  ![iframe](assert/w3cshool.png)

## 二、版本说明

### 2.1 当前版本说明

当前版本(分支：release_simple)提供最基础的权限控制，使用 JdbcTemplate，减少过度的技术使用，方便初学者学习。

### 2.2 后续版本计划

* 字典表：前端组件封装结合字典 + 字典表管理


* 权限颗粒完善：列权限控制、数据（行）权限控制（可扩展）
* 安全：数据脱敏、加解密

## 三、发布版本

* [release_simple](https://gitee.com/laohuangshu/hoo-platform-parent/tree/release_simple) 提供基础的权限支持，技术栈较少，适合入门学习。